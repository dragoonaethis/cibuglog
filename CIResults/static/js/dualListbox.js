const RENDER_BUFF_SIZE = 25

const filter = (group, filterValue) => {
  let filtered = []
  const re = new RegExp(filterValue)
  for (let i = 0; i < group.options.length; i++) {
    if (!group.options[i]) {
      continue;
    }
    if (re.test(group.options[i].text)) {
      filtered.push(group.options[i])
    }
  }
  group.filtered = filtered
  group.counter.filtered = filtered.length
}

const refreshCounter = (group) => {
  let counter;
  if (group.inputFilter.value) {
    counter = `Filtered ${group.counter.filtered}/${group.counter.all}`
  } else {
    counter = `Showing all ${group.counter.all}`
  }
  group.countLabel.textContent = counter
}

const createGroup = (name) => {
  const group = {
    name: name,
    options: [],
    filtered: [],
    list: document.createElement("ul"),
    inputFilter: document.createElement("input"),
    moveButton: document.createElement("button"),
    moveAllButton: document.createElement("button"),
    selected: [],
    counter: {
      filtered: 0,
      all: 0
    },
    lastRenderedIndex: -1,
    lastRenderedNumber: 0,
    anchorIndex: 0,
    lastFocusIndex: -1,
    countLabel: document.createElement("small"),
  }
  group.list.classList.add("list-group", "list-group-flush")
  group.inputFilter.classList.add("form-control", "mb-2", "py-1")
  group.inputFilter.placeholder = "Filter"

  group.moveButton.classList.add("btn", "btn-outline-secondary", "btn-sm")
  group.moveButton.textContent = "Move"
  group.moveAllButton.classList.add("btn", "btn-outline-secondary", "btn-sm")
  group.moveAllButton.textContent = "Move all"
  group.countLabel.style.display = "block"
  group.countLabel.style.fontSize = "11px"
  return group
}

const getOppositeGroupName = (sourceGroupName) => {
  return sourceGroupName == "available" ? "selected" : "available"
}

const select = (group, index) => {
  group.selected.push(group.options[index]);
  group.options[index].liElement.dataset.selected = "true";
  group.options[index].liElement.classList.add("list-group-item-primary");
};

const unselect = (group, index) => {
  group.selected.splice(group.selected.indexOf(group.options[index]), 1)
  group.options[index].liElement.dataset.selected = "false"
  group.options[index].liElement.classList.remove("list-group-item-primary")
}

const createGroupContainer = (group) => {
  const container = document.createElement("div")
  container.style.flex = "1 1 0"
  container.style.width = "0"
  const label = document.createElement("span")
  label.textContent = group.name
  container.appendChild(label)
  container.appendChild(group.countLabel)
  container.appendChild(group.inputFilter)
  const buttonGroup = document.createElement("div")
  buttonGroup.classList.add("btn-group")
  buttonGroup.appendChild(group.moveButton)
  buttonGroup.appendChild(group.moveAllButton)
  const listContainer = document.createElement("div")
  listContainer.classList.add("card")
  listContainer.appendChild(buttonGroup)
  listContainer.appendChild(group.list)
  group.list.style.height = "15rem"
  group.list.style.overflow = "scroll"
  container.appendChild(listContainer)
  container.classList.add("m-2")
  return container
}

const getOptions = (group) => {
  if (group.inputFilter.value) {
    return group.filtered
  }
  return group.options
}

const render = (group, numToRender) => {
  let rendered = 0;
  const options = getOptions(group)
  for (let i = group.lastRenderedIndex + 1; i < options.length && rendered < numToRender; i++) {
    if (!options[i]) {
      continue
    }
    rendered += 1
    group.list.appendChild(options[i].liElement)
    group.lastRenderedIndex = i
  }
  return rendered
}

const renderNext = (group, numToRender) => {
  group.lastRenderedNumber = render(group, numToRender)
}

const refreshRender = (group) => {
  const numToRender = RENDER_BUFF_SIZE - group.lastRenderedNumber
  group.lastRenderedNumber += render(group, numToRender)
}

const shouldRenderNext = (group) => {
  const offset = group.list.scrollHeight - group.list.offsetHeight
  return group.list.scrollTop > offset - RENDER_BUFF_SIZE * 10
}

class DualListbox {
  constructor(originalSelect) {
    this.originalSelect = originalSelect
    this.available = createGroup("Available")
    this.selected = createGroup("Selected")
    this.container = document.createElement("div")
    this.container.style.display = "flex"
    this.container.appendChild(createGroupContainer(this.available))
    this.container.appendChild(createGroupContainer(this.selected))

    const setupGroup = (group) => {
      group.inputFilter.addEventListener("input", event => {
        filter(group, event.target.value);
        group.lastRenderedNumber = 0
        group.lastRenderedIndex = -1
        group.list.replaceChildren()
        renderNext(group, RENDER_BUFF_SIZE)
        refreshCounter(group)
      })
      group.moveButton.addEventListener("click", _ => {
        for (let i = 0; i < group.selected.length; i++) {
          group.selected[i].liElement.dataset.selected = "false"
          group.selected[i].liElement.classList.remove("list-group-item-primary")
          this.moveElement(group.selected[i])
        }
        this.refreshGroups()
        group.selected = []
      })

      group.moveAllButton.addEventListener("click", _ => {
        const options = getOptions(group)
        for (let i = 0; i < options.length; i++) {
          if (!options[i]) {
            continue
          }
          this.moveElement(options[i])
        }
        if (group.inputFilter.value) {
          group.filtered = []
        } else {
          group.options = []
        }
        group.counter.filtered = 0
        group.lastRenderedIndex = -1
        group.lastRenderedNumber = 0
        this.refreshGroups()
      })

      group.list.addEventListener("scroll", _ => {
        if (shouldRenderNext(group)) {
          renderNext(group, RENDER_BUFF_SIZE)
        }
      })
    }

    setupGroup(this.available)
    setupGroup(this.selected)


    this.populate()
    renderNext(this.available, RENDER_BUFF_SIZE)
    renderNext(this.selected, RENDER_BUFF_SIZE)
    this.available.counter.all = this.available.options.length
    this.selected.counter.all = this.selected.options.length
    refreshCounter(this.selected)
    refreshCounter(this.available)
  }

  getGroup(name) {
    return name == "available" ? this.available : this.selected
  }

  refreshGroups() {
    const groups = [this.available, this.selected]
    groups.forEach(group => {
      refreshRender(group)
      refreshCounter(group)
    })
  }

  moveElement(option) {
    const addOptionToGroup = (group, option) => {
      const toGroupName = getOppositeGroupName(option.liElement.dataset.group)
      group.counter.all += 1
      option.liElement.dataset.group = toGroupName
      option.liElement.dataset.index = group.options.push(option) - 1
      if (group.inputFilter.value) {
        const re = RegExp(group.inputFilter.value)
        if (re.test(option.text)) {
          group.counter.filtered += 1
          group.filtered.push(option)
        }
      }
      this.originalSelect.options[option.liElement.dataset.selectIndex].selected = toGroupName == "selected" ? true : false
    }

    const removeOptionFromGroup = (group, option) => {
      group.counter.all -= 1
      if (option.liElement.dataset.selected == "true") {
        unselect(group, option.liElement.dataset.index)
      }
      group.options[option.liElement.dataset.index] = null
      if (option.liElement.isConnected) {
        group.list.removeChild(option.liElement)
      }
      if (option.liElement.dataset.index == group.anchorIndex) {
        group.anchorIndex = 0;
      }
      if (option.liElement.dataset.index == group.lastFocusIndex) {
        group.lastFocusIndex = -1;
      }
    }

    const toGroupName = getOppositeGroupName(option.liElement.dataset.group)
    const fromGroup = this.getGroup(option.liElement.dataset.group)
    const toGroup = this.getGroup(toGroupName)
    removeOptionFromGroup(fromGroup, option)
    addOptionToGroup(toGroup, option)
  }

  populate() {
    for (let i = 0; i < this.originalSelect.options.length; i++) {
      const option = this.originalSelect.options[i]
      const li = document.createElement("li")
      li.textContent = option.text
      li.dataset.selectIndex = i
      li.dataset.selected = "false"
      li.classList.add("list-group-item", "list-group-item-action", "p-1", "text-nowrap")

      li.addEventListener("dblclick", (event) => {
        if (event.shiftKey) {
          return;
        }
        const option = this.getGroup(event.target.dataset.group).options[event.target.dataset.index];
        this.moveElement(option);
        this.refreshGroups();
      });

      li.addEventListener("click", (event) => {
        const group = this.getGroup(event.target.dataset.group);
        const targetIndex = event.target.dataset.index;
        if (event.shiftKey) {
          const anchorNodeIndex = group.anchorIndex;
          if (targetIndex !== anchorNodeIndex) {
            window.getSelection().removeAllRanges();
          }
          const selectRangeBegin = Math.min(anchorNodeIndex, targetIndex);
          const selectRangeEnd = Math.max(anchorNodeIndex, targetIndex);
          for (let i = selectRangeBegin; i <= selectRangeEnd; i++) {
            if (
              group.options[i] !== null &&
              group.options[i].liElement.dataset.selected == "false"
            ) {
              select(group, i);
            }
          }
          if (group.lastFocusIndex === -1) {
            group.lastFocusIndex = targetIndex;
            return;
          }
          let unselectRangeBegin, unselectRangeEnd;
          if (selectRangeEnd < group.lastFocusIndex) {
            unselectRangeBegin = selectRangeEnd + 1;
            unselectRangeEnd = group.lastFocusIndex;
          } else if (group.lastFocusIndex < selectRangeBegin) {
            unselectRangeBegin = group.lastFocusIndex;
            unselectRangeEnd = selectRangeBegin - 1;
          }
          for (let i = unselectRangeBegin; i <= unselectRangeEnd; i++) {
            if (
              group.options[i] !== null &&
              group.options[i].liElement.dataset.selected == "true"
            ) {
              unselect(group, i);
            }
          }
          group.lastFocusIndex = targetIndex;
        } else {
          group.anchorIndex = targetIndex;
          group.lastFocusIndex = -1;
          if (event.target.dataset.selected == "true") {
            unselect(group, targetIndex);
          } else {
            select(group, targetIndex);
          }
        }
      });

      if (option.selected) {
        li.dataset.group = "selected"
        li.dataset.index = this.selected.options.push({ value: option.value, text: option.text, liElement: li }) - 1
      } else {
        li.dataset.group = "available"
        li.dataset.index = this.available.options.push({ value: option.value, text: option.text, liElement: li }) - 1
      }
    }
    renderNext(this.available, RENDER_BUFF_SIZE)
    renderNext(this.selected, RENDER_BUFF_SIZE)
    this.available.counter.all = this.available.options.length
    this.selected.counter.all = this.selected.options.length
    refreshCounter(this.selected)
    refreshCounter(this.available)
  }

  reset() {
    this.available.list.replaceChildren()
    this.selected.list.replaceChildren()
    this.available.options = []
    this.selected.options = []
    this.available.lastRenderedIndex = -1
    this.selected.lastRenderedIndex = -1
    this.populate()
  }
}

const initDualListbox = (select) => {
  select.style.display = "none"
  const dualListbox = new DualListbox(select)
  select.insertAdjacentElement("beforebegin", dualListbox.container);
  select.dualListbox = dualListbox
}
